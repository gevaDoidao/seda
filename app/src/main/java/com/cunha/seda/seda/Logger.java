package com.cunha.seda.seda;

import android.util.Log;

/**
 * Created by edupa on 30/10/2017.
 */

public class Logger {

    public void log(String where, String message){
        Log.d(where, message);
    }

}
