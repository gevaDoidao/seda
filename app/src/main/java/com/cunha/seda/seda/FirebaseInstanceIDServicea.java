package com.cunha.seda.seda;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by edupa on 30/10/2017.
 */

public class FirebaseInstanceIDServicea extends FirebaseInstanceIdService {

    private static String TAG = "FirebaseInstanceID";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        new Logger().log(TAG, "Token: "+refreshedToken);
    }
}
