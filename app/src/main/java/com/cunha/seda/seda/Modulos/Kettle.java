package com.cunha.seda.seda.Modulos;

import android.graphics.drawable.Drawable;

import com.cunha.seda.seda.R;

/**
 * Created by edupa on 02/12/2017.
 */

public class Kettle implements Modulo {
    private static String name = "Kettle";

    @Override
    public String getUuid() {
        return null;
    }

    @Override
    public void setUuid(String uuid) {

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public int getKind() {
        return 0;
    }

    @Override
    public void setKind(int kind) {

    }

    @Override
    public int getIcon() {
        return R.drawable.round_teapot;
    }

    @Override
    public int getNotifications() {
        return 0;
    }
}
