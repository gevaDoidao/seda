package com.cunha.seda.seda.Activities;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.cunha.seda.seda.HTTPConnector;
import com.cunha.seda.seda.Modulos.Modulo;
import com.cunha.seda.seda.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by edupa on 15/11/2017.
 */

public class GetHistoric extends AsyncTask<String , Void ,String> {
    String server_response;
    Context context;

    HTTPConnector.ResponseListener listener;

    public GetHistoric(HTTPConnector.ResponseListener listener, Context context) {
        this.listener = listener;
        this.context = context;
    }

    @Override
    protected String doInBackground(String... strings) {

        URL url;
        HttpURLConnection urlConnection = null;

        try {
            url = new URL(strings[0]);
            urlConnection = (HttpURLConnection) url.openConnection();

            int responseCode = urlConnection.getResponseCode();

            if(responseCode == HttpURLConnection.HTTP_OK){
                server_response = readStream(urlConnection.getInputStream());
                Log.v("CatalogClient", server_response);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        try {
            if(server_response != null) {
                JSONObject jsonObject = new JSONObject(server_response);
                JSONArray jsonArray = jsonObject.getJSONArray("historico");
                //ArrayList<Modulo> modulesList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject moduloJson = jsonArray.getJSONObject(i);
                    Log.d("GH", "id: "+String.valueOf(moduloJson.getInt("id")));
                    Log.d("GH", "modulo_id: "+String.valueOf(moduloJson.getInt("modulo_id")));
                    Log.d("GH", "time_stamp: "+String.valueOf(moduloJson.getInt("time_stamp")));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Log.e("Response", "" + server_response);


    }
    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder response = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }
}
