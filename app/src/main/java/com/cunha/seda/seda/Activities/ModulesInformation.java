package com.cunha.seda.seda.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.cunha.seda.seda.Constants;
import com.cunha.seda.seda.HTTPConnector;
import com.cunha.seda.seda.Modulos.Modulo;
import com.cunha.seda.seda.R;

import java.util.ArrayList;

public class ModulesInformation extends AppCompatActivity implements HTTPConnector.ResponseListener{

    TextView name, kind;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modules_information);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initViews();
        getBundle();
        setInformation();

        new  GetHistoric(this, this).execute("http://192.168.25.18/historico?id=0");
    }
    private void initViews(){
        name = (TextView)findViewById(R.id.name);
        kind = (TextView)findViewById(R.id.kind);
    }
    private void getBundle(){
        bundle = getIntent().getExtras();
    }

    private void setInformation(){
        name.setText(bundle.getString(Constants.Name));
        kind.setText(Constants.kindList[bundle.getInt(Constants.Kind)]);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponseSuccessfull(ArrayList<Modulo> modulesList) {

    }
}
