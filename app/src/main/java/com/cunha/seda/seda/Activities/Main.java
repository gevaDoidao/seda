package com.cunha.seda.seda.Activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.cunha.seda.seda.Constants;
import com.cunha.seda.seda.HTTPConnector;
import com.cunha.seda.seda.Logger;
import com.cunha.seda.seda.Modulos.Modulo;
import com.cunha.seda.seda.PrimaryListAdapter;
import com.cunha.seda.seda.R;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;

public class Main extends AppCompatActivity implements HTTPConnector.ResponseListener {


    ListView listView;
    HTTPConnector httpConnector;
    ArrayList<Modulo> modulesList;

    @Override
    protected void onResume() {
        super.onResume();
        new GetModulos(this,this).execute("http://192.168.25.18/modulos?user=1234");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        FirebaseMessaging.getInstance().subscribeToTopic("teste");
    }

    private void initViews(){
        listView = (ListView)findViewById(R.id.listView);
    }

    @Override
    public void onResponseSuccessfull(ArrayList<Modulo> modulesList) {
        new Logger().log("Main.class", "Modulos encontrados: "+modulesList.size());
        this.modulesList = modulesList;
        PrimaryListAdapter adapter = new PrimaryListAdapter(modulesList, this);
        listView.setAdapter(adapter);
    }

    public void openModule(int position){
        Intent intent = new Intent(this, ModulesInformation.class);
        intent.putExtra(Constants.Name, modulesList.get(position).getName());
        intent.putExtra(Constants.Kind, modulesList.get(position).getKind());
        startActivity(intent);
    }
}
