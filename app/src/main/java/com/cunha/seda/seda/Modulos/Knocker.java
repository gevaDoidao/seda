package com.cunha.seda.seda.Modulos;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.cunha.seda.seda.R;

/**
 * Created by edupa on 02/12/2017.
 */

public class Knocker implements Modulo {

    private String uuid;
    private String name;
    private String tipo;

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getKind() {
        return 0;
    }

    @Override
    public void setKind(int kind) {

    }

    @Override
    public int getIcon() {
        return R.drawable.open_exit_door;
    }

    @Override
    public int getNotifications() {
        return 0;
    }
}
