package com.cunha.seda.seda;

import android.graphics.drawable.Drawable;

/**
 * Created by edupa on 30/10/2017.
 */

public class Constants {

    public static String PrimaryList = "primary_list";
    public static String Name = "name";
    public static String Kind = "kind";

    public static String[] kindList = {"Knocker", "TeaCup", "Sinalizer"};
    public static int[] drawableList = {R.drawable.open_exit_door, R.drawable.round_teapot, R.drawable.flashlight};

}
