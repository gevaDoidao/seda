package com.cunha.seda.seda.Modulos;

import android.graphics.drawable.Drawable;

/**
 * Created by edupa on 02/12/2017.
 */

public interface Modulo {
    String getUuid();
    void setUuid(String uuid);
    String getName();
    void setName(String name);
    int getKind();
    void setKind(int kind);
    int getIcon();
    int getNotifications();
}
