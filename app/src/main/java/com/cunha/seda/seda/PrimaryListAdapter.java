package com.cunha.seda.seda;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cunha.seda.seda.Activities.Main;
import com.cunha.seda.seda.Modulos.Modulo;

import java.util.ArrayList;

/**
 * Created by edupa on 30/10/2017.
 */

public class PrimaryListAdapter extends BaseAdapter implements View.OnClickListener{

    private ArrayList<Modulo> modules;
    private Context context;
    LayoutInflater inflater;
    private Main main;

    public PrimaryListAdapter(ArrayList<Modulo> modules, Context context) {
        this.modules = modules;
        this.context = context;
        this.main = (Main)context;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return modules.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = inflater.inflate(R.layout.primary_list_rows, null);
        v.setOnClickListener(this);
        v.setTag(position);
        TextView name = (TextView)v.findViewById(R.id.name);
        TextView kind = (TextView)v.findViewById(R.id.kind);
        ImageView icon = (ImageView)v.findViewById(R.id.icon);
        TextView notification = (TextView)v.findViewById(R.id.notifications);
        icon.setImageDrawable(ContextCompat.getDrawable(context, modules.get(position).getIcon()));
        name.setText(modules.get(position).getName());
        kind.setText(Constants.kindList[modules.get(position).getKind()]);

        if(modules.get(position).getNotifications()==0){
            notification.setVisibility(View.VISIBLE);
            notification.setText("1");
        }
        return v;
    }

    @Override
    public void onClick(View v) {
        main.openModule((int)v.getTag());
    }
}
