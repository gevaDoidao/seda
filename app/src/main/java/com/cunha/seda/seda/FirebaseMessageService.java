package com.cunha.seda.seda;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by edupa on 30/10/2017.
 */

public class FirebaseMessageService extends FirebaseMessagingService {

    private static String firebaseClass = "FirebaseService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        new Logger().log(firebaseClass, "From: "+remoteMessage.getFrom());

        if(remoteMessage.getData().size() > 0){
            new Logger().log(firebaseClass, "Data: "+remoteMessage.getData().toString());
        }

        if(remoteMessage.getNotification() != null){
            new Logger().log(firebaseClass, "Notification: "+remoteMessage.getNotification().getBody());
        }
    }
}
